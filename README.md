# Hodoor ansible inventories

This repository stores hosts informations and related variables for this specific instance of Odoo.

:warning: This instance is only for testing purposes. :warning:

## Instructions

1. Clone this repo and [odoo-provisioning](https://gitlab.com/coopdevs/odoo-provisioning) in the same directory
2. If you want to test this set up locally, install [devenv](https://github.com/coopdevs/devenv/) and do:
   ```sh
   cd odoo-hodoor-inventory
   devenv # this creates the lxc container and sets its hostname
   ```
3. Go to `odoo-provisioning` directory and checkout to the version supported by the inventory.
   You can find the var `odoo_provisioning_version` with this information.
4. Install its Ansible dependencies:
   ```sh
   ansible-galaxy install -r requirements.yml
   ```
4. Run `ansible-playbook` command pointing to the `inventory/hosts` file of this repository:
   * development local mode
   ```sh
   # tell it to keep it local with limit=dev
   # use the user root the first time to create the other users: --user=root
   ansible-playbook playbooks/sys_admins.yml -i ../odoo-hodoor-inventory/inventory/hosts --limit=dev
   ansible-playbook playbooks/provision.yml -i ../odoo-hodoor-inventory/inventory/hosts --limit=dev
   ```

5. Follow the instructions in the Hodoor module to start the server:
https://gitlab.com/coopdevs/odoo-hodoor/-/tree/master/hodoor#development

6. Access to http://odoo-hodoor.local:8069
